/**
 * (C) Copyright 2017 Falk Boudewijn Schimweg - All rights reserved.
 * You may not distribute, change or reuse any parts of this code.
 */
var dir  = "";
var hist = [];
var histIndex = -1;
var currCMD = null;
var loggedIn = false;

$(document).ready(function(){
	// Init
	focusCMD();
	dir = $("#path").text();
	$("#path").html("Enter password:&nbsp;");
	document.title = dir;
	
	// Listeners
	initListeners();
});

function initListeners(){
	$(document).keydown(focusCMD);
	
	// Enter-Listener
	$("#input").keydown(function(handler){
		if ( handler.which == 13) {
			// Stop propagation
			handler.stopPropagation();
			handler.preventDefault();
			
			if(loggedIn){
				// Execute Command
				var cmd = getCMD();
				if (cmd!=""){
					exec(cmd);
				} else {
					printLine($("#path").html()+"&gt;&nbsp;"+$("#input").html());
				}
			} else {
				var pass = getCMD();
				login(pass);
			}
			
			// Reset input
			if(loggedIn && cmd.trim()!=""){
				hist.unshift(cmd);
				histIndex = -1;
			}
			return false;
		}
	});
}

// Shortcuts
$(window).keydown(function(event) {
	if(event.ctrlKey && event.keyCode == 67) { 
		console.log("Ctrl+C");
		if(currCMD!=null){
			currCMD.abort();
		}
		printLine($("#path").html()+"&gt;&nbsp;"+$("#input").html()+"^C");
		clearInput();
		event.preventDefault(); 
	}
	if(event.ctrlKey && event.keyCode == 76) { 
		console.log("Ctrl+L");
		clearCMD();
		event.preventDefault(); 
	}
	if(event.keyCode == 38){
		console.log('Key up');
		if(histIndex+1<hist.length){
			histIndex++;
			$("#input").html(hist[histIndex]);
		}
		event.preventDefault();
	}
	if(event.keyCode == 40){
		if(histIndex-1>0){
			histIndex--;
			$("#input").html(hist[histIndex]);
		} else {
			histIndex=-1;
			$("#input").html('');
		}
		event.preventDefault();
	}
	if(event.keyCode == 8){
		if(!$('#input').is(":focus")){
			event.preventDefault();
		}
	}
});

function enableInput(){
	document.getElementById('input').contentEditable='true';
	focusCMD();
}

function disableInput(){
	document.getElementById('input').contentEditable='false';
}

function exec(cmd){
	var errCode;
	disableInput();
	currCMD = $.post({
		url : "exec.php",
		data: {cmd: cmd, dir: dir},
		success: function(ret){errCode=ret.return; getReturn(ret);},
		complete: enableInput});
	return errCode;
}

function login(pass){
	var errCode;
	disableInput();
	currCMD = $.post({
		url : "login.php",
		data: {password: pass},
		success: function(ret){errCode=ret.return; getReturn(ret);},
		complete: enableInput});
	return errCode;
}

function printLine(line){
	$($.parseHTML(line+"<br/>")).insertBefore($("#curr-line"));
	// Scroll bottom
	$(window).scrollTop($("#console").height());
}

function getReturn(data){
	if(loggedIn){
		printLine($("#path").html()+"&gt;&nbsp;"+$("#input").html());
		dir=data.dir;
		updateDir();
	} else {
		if(data.return==0){
			loggedIn=true;
			$('#input').removeClass('blackout');
			dir=data.dir;
			updateDir();
		} else {
			printLine("Enter password:&nbsp;");
		}
	}
	clearInput();
	if(data.output==undefined){
		console.log(data);
		return;
	}
	for(var i=0; i<data.output.length; i++){
		printLine(data.output[i]);
	}
}

function clearCMD(){
	$("#console").html($("#curr-line"));
	initListeners();
	focusCMD();
}

function clearInput(){
	$("#input").html("");
}

function focusCMD(){
	$("#input").get(0).focus();
}

function updateDir(){
	$("#path").html(dir);
	document.title = dir;
}

function getCMD(){
	return $("#input").text().trim();
};