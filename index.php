<?php session_start(); $_SESSION['loggedIn'] = false; ?>
<!DOCTYPE HTML>
<!--
 (C) Copyright 2017 Falk Boudewijn Schimweg - All rights reserved.
 You may not distribute, change or reuse any parts of this code.
-->
<html>
<head>
<title></title>
<link rel="icon" href="fav.png" />
<link rel="stylesheet" href="css/default.css" />
<script src="js/jquery.min.js" ></script>
<script src="js/connection.js" ></script>
</head>
<body>
<div id="console">
<span id="curr-line"><span id="path"></span>&gt;&nbsp;<span id="input" class="blackout" contenteditable></span></span>
</div>
</body>
</html>
