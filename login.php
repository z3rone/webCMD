<?php
/**
 * (C) Copyright 2017 Falk Boudewijn Schimweg - All rights reserved.
 * You may not distribute, change or reuse any parts of this code.
 */
session_start();
header("Content-Type: application/json;");

define('PASSWORD', '3c29c81650fa54d5cd2c522b2190e72b73cc0accab33d4ede6c0814e39ed4a77');

if (hash("sha256", $_REQUEST['password']) == PASSWORD){
	$_SESSION['loggedIn'] = true;
	echo json_encode(['output' => array('Logged in!'), 'return' => 0, 'dir' => getcwd()]);
} else {
	echo json_encode(['output' => array('Wrong password!'), 'return' => 1, 'dir' => getcwd()]);
}